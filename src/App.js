import React, { useEffect, useState } from 'react';
import styled from '@emotion/styled';
import Frase from './components/Frase';
const Contenedor = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  align-self: flex-end;
`;
const Contenido = styled.div`
  margin: 60vh auto;
`;
const Boton = styled.button`
  background-color: #314e52;
  color: #fff;
  border: none;
  padding: 10px;
  text-align: center;
  border-radius: 5px;
  width: 200px;
  text-transform: uppercase;
  font-weight: 700;
  cursor: pointer;
`;

function App() {
  const [data, setData] = useState({});
  const handleOnClick = async () => {
    const api = 'https://breaking-bad-quotes.herokuapp.com/v1/quotes';
    const frase = await fetch(api);
    const respuesta = await frase.json();
    setData(respuesta[0]);
  };
  useEffect(() => {
    handleOnClick();
  }, []);
  return (
    <Contenedor>
      <Contenido>
        <Boton onClick={handleOnClick}>Obtener Frase</Boton>
        <Frase data={data} />
      </Contenido>
    </Contenedor>
  );
}

export default App;
