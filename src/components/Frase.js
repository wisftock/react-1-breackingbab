import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';

const Container = styled.div`
  width: 80%;
  padding: 10px;
  /* margin: auto; */
  border-radius: 5px;
  background-color: #fff;
`;
const Blockquote = styled.blockquote`
  font-weight: 700;
`;
const Cite = styled.cite`
  /* margin-right: 0px; */
  text-align: right;
  /* font-weight: 300; */
  /* text-align: right; */
`;
const Frase = ({ data }) => {
  const { author, quote } = data;
  return (
    <Container>
      <Blockquote>“{quote}”</Blockquote>
      <Cite>-{author}</Cite>
    </Container>
  );
};
Frase.propTypes = {
  frase: PropTypes.object.isRequired,
};
export default Frase;
